package paquete1;

import java.util.Objects;
import java.util.Set;

public class Trabajador {
	
	String  nombre;
	int calificacion;
	String rol;
	public int codigo;
	
	Set<Trabajador> incompatibles;
	
	
	public Trabajador() {}
	public Trabajador(String nom, int cali, String r, Set<Trabajador> losOdio, int codigo){
		
		if(cali < 1 || cali > 5) {
			throw new RuntimeException("la calificacion del trabajador no es un valor aceptable");
		}
		//if(rol != "lider de proyecto" && rol != "programador" && rol != "arquitecto" && rol != "tester") {
		//	throw new RuntimeException("el rol del empleado no es uno valido");
		//}
		
		this.nombre = nom;
		this.calificacion = cali;
		this.rol = r;
		this.incompatibles = losOdio;
		this.codigo = codigo;
	}

	@Override
	public String toString() {
		return "Trabajador [nombre=" + nombre + ", calificacion=" + calificacion + ", rol=" + rol + "]";
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trabajador other = (Trabajador) obj;
		return calificacion == other.calificacion && Objects.equals(incompatibles, other.incompatibles)
				&& Objects.equals(nombre, other.nombre) && Objects.equals(rol, other.rol);
	}

	
	// se creo el getRol para simplificar el codigo.
	public String getRol() {
		return rol;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	

	
	

}
