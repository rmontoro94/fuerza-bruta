package paquete1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class MetodosTest {

	
	// auxiliar
	Set<Trabajador> pocoPopulares(){
		Set<Trabajador> parias = new HashSet<Trabajador>();
		
		parias.add(new Trabajador("fede", 1, "programador", parias, 1000));
		parias.add(new Trabajador("tomas", 1, "programador", parias, 1001));
		return parias;
		
	}
	
	Set<Trabajador> conjuntoTrabajadores() {
		
		Set<Trabajador> trabajadores = new HashSet<Trabajador>();
		
		trabajadores.add(new Trabajador("pedro", 3, "programador", pocoPopulares(), 1002));
		
		trabajadores.add(new Trabajador("pedro", 2, "programador", pocoPopulares(), 1003));
		trabajadores.add(new Trabajador("pedro", 1, "programador", pocoPopulares(), 1004));
		trabajadores.add(new Trabajador("pedro", 4, "programador", pocoPopulares(), 1005));
		trabajadores.add(new Trabajador("pedro", 5, "programador", pocoPopulares(), 1006));
		trabajadores.add(new Trabajador("juan", 5, "programador", pocoPopulares(), 1007));
		
		return trabajadores;
	}
	
	// una instancia de Solver para poder acceder a sus metodos y testearlos
	Solver conjunto() {
		Trabajador[] todosLTrabajadores = new Trabajador[6];
		
		todosLTrabajadores[0] = new Trabajador("pedro", 3, "programador", pocoPopulares(), 1000);
		todosLTrabajadores[1] = new Trabajador("pedro", 2, "programador", pocoPopulares(), 1001);
		todosLTrabajadores[2] = new Trabajador("pedro", 1, "programador", pocoPopulares(), 1002);
		todosLTrabajadores[3] = new Trabajador("pedro", 4, "programador", pocoPopulares(), 1003);
		todosLTrabajadores[4] = new Trabajador("pedro", 5, "programador", pocoPopulares(), 1004);
		todosLTrabajadores[5] = new Trabajador("juan", 5, "programador", pocoPopulares(), 1005);
		
		Solver elSolver = new Solver(todosLTrabajadores);
		
		return elSolver;
	}
	
	
	@Test
	public void testCompatibles() {
		// set up
		Solver grupo = conjunto();
		
		// exercise and verify
		assertTrue(grupo.grupoCompatible(conjuntoTrabajadores()));
	}
	
	@Test
	public void testIncompatibles() {
		
		// set up
		Solver grupo = conjunto();
		
		Set<Trabajador> trabajadores = new HashSet<Trabajador>();
		
		// los incompatibles
		Set<Trabajador> losMalos = new HashSet<Trabajador>();
		Trabajador malo1 = new Trabajador("fede", 1, "programador", pocoPopulares(), 1000);
		Trabajador malo2 = new Trabajador("fede", 1, "programador", pocoPopulares(), 1001);
		losMalos.add(malo1);
		losMalos.add(malo2);
		
		trabajadores.add(new Trabajador("pedro", 3, "programador", losMalos, 1002));
		trabajadores.add(new Trabajador("pedro", 2, "programador", losMalos, 1003));
		trabajadores.add(new Trabajador("pedro", 1, "programador", losMalos, 1004));
		trabajadores.add(new Trabajador("pedro", 4, "programador", losMalos, 1005));
		trabajadores.add(malo1);
		trabajadores.add(malo2);
		
		// exercise and verify
		assertFalse(grupo.grupoCompatible(trabajadores));
		
	}
	
	@Test
	public void testValorDeEquipo() {
	
		// set up
		Solver grupo = conjunto();
		Set<Trabajador> grupo0 = new HashSet<Trabajador>();
		
		// exercise and verify
		
		// el grupo generico de ejemplo deberia de tener un valor de 20
		assertEquals(20, grupo.valorEquipo(conjuntoTrabajadores() ));
		// grupo vacio tiene un valor de 0
		assertEquals(0, grupo.valorEquipo(grupo0));	
	}
	@Test
	public void testGrupoValido() {
		
		// set up
		Solver grupo = conjunto();
		
		Set<Trabajador> unGrupoValido = new HashSet<Trabajador>();
		unGrupoValido.add(new Trabajador("pedro", 2, "Lider de proyecto", pocoPopulares(), 1000));
		unGrupoValido.add(new Trabajador("pedro", 1, "Arquitecto", pocoPopulares(), 1001));
		unGrupoValido.add(new Trabajador("pedro", 4, "Programador", pocoPopulares(), 1002));
		unGrupoValido.add(new Trabajador("pedro", 5, "Tester", pocoPopulares(), 1000));
		
		Set<Trabajador> grupoNoValido = new HashSet<Trabajador>();
		grupoNoValido.add(new Trabajador("pedro", 2, "Lider de proyecto", pocoPopulares(), 1003));
		grupoNoValido.add(new Trabajador("pedro", 1, "Arquitecto", pocoPopulares(), 1004));
		grupoNoValido.add(new Trabajador("pedro", 4, "Programador", pocoPopulares(), 1005));
		
		Set<Trabajador> grupoVacio = new HashSet<Trabajador>();
		
		// exercise and verify
		// todas las pruebas se hacen pidiendo al menos 1 miembro de cada rol.
		assertTrue(grupo.grupoValido(unGrupoValido, 1, 1, 1, 1));
		assertFalse(grupo.grupoValido(grupoNoValido, 1, 1, 1, 1));
		assertFalse(grupo.grupoValido(grupoVacio, 1, 1, 1, 1));
		
	}
	
}
