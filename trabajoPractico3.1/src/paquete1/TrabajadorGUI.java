package paquete1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

public class TrabajadorGUI extends JFrame {
    private JTextField nombreField;
    private JTextField categoriaField;
    private JTextField nivelField;
    private JTextField IDField;
    private JButton agregarButton;
    private JButton agregarPariaButton;
    private Set<Trabajador> trabajadores;
    private Trabajador[] todosLTrabajadores;
    private int contadorTrabajadores;
    private JFrame frame;

    public TrabajadorGUI() {
        trabajadores = new HashSet<>();
        
        Set<Trabajador> parias = new HashSet<Trabajador>();
        
        todosLTrabajadores = new Trabajador[8];
        contadorTrabajadores = 0;
        

        setTitle("Ingreso de Trabajadores");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(6, 2));

        JLabel nombreLabel = new JLabel("Nombre:");
        nombreField = new JTextField();
        JLabel categoriaLabel = new JLabel("Categoría:");
        categoriaField = new JTextField();
        JLabel nivelLabel = new JLabel("Nivel:");
        JLabel IDLabel = new JLabel("Nivel:");
        nivelField = new JTextField();
        IDField = new JTextField();
        agregarButton = new JButton("Agregar");
        agregarPariaButton = new JButton("Agregar Paria");
        Solver solver = new Solver(todosLTrabajadores);
        Thread thread = new Thread(solver);
        thread.start();
        JButton botonIncompatibilidades = new JButton("Imprimir Incompatibilidades");
        JButton generarEquipoButton = new JButton("Generar Equipo");
        JButton todosLosTrabajadoresButton = new JButton("TODOS_LOS_TRABAJADORES");
        
        
        //Botton agregar trabajadores
        agregarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = nombreField.getText();
                String categoria = categoriaField.getText();
                int nivel = Integer.parseInt(nivelField.getText());
                //int ID = Integer.parseInt(IDField.getText());
                int ID = 1;
                Set<Trabajador> setVacio = new HashSet<Trabajador>();

                Trabajador trabajador = new Trabajador(nombre, nivel, categoria, setVacio, ID);
                trabajadores.add(trabajador);
                todosLTrabajadores[contadorTrabajadores] = trabajador;
                contadorTrabajadores++;

                if (contadorTrabajadores == todosLTrabajadores.length) {
                    // Se han ingresado todos los trabajadores, realizar el procesamiento
                    

                    Main.imprimirRequerimientosEquipo(solver);
                    
                    // Cerrar la ventana después de mostrar los resultados
                    dispose();
                } else {
                    // Limpiar los campos para ingresar el siguiente trabajador
                    nombreField.setText("");
                    categoriaField.setText("");
                    nivelField.setText("");
                }
            }
        });
        
        
        //Boton generar equipo y mostrar
        
        generarEquipoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	
            	Main.imprimirMejorEquipo(solver);
                Main.imprimirCombinacionesGeneradas(solver);
            }
        });
        
        
        //Boton mostrar todos los trabajadores
        
        todosLosTrabajadoresButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.imprimirTrabajadores(solver._personas);
            }
        });

        frame.getContentPane().add(todosLosTrabajadoresButton);
        
        
        
        
        //Boton agregar paria
        agregarPariaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input1 = JOptionPane.showInputDialog("Ingrese el ID del trabajador:");
                String input2 = JOptionPane.showInputDialog("Ingrese el ID del paria:");

                if (input1 != null && input2 != null) {
                    try {
                        int trabajador1 = Integer.parseInt(input1);
                        int trabajador2 = Integer.parseInt(input2);

                        Trabajador t1 = buscarTrabajador(trabajador1);
                        Trabajador t2 = buscarTrabajador(trabajador2);

                        if (t1 != null && t2 != null) {
                            parias.add(t2);
                            JOptionPane.showMessageDialog(null, "Se agregó el paria correctamente.");
                        } else {
                            JOptionPane.showMessageDialog(null, "No se encontró uno o ambos trabajadores.");
                        }
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Ingrese un número entero válido para el ID.");
                    }
                }
            }
        });
        
        
        //Boton mostrar parias
        //  evento de clic 
        // preguntarle el ID de un trabajador
        botonIncompatibilidades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String input = JOptionPane.showInputDialog("Ingrese el ID:");
                if (input != null) {
                    try {
                        int ID = Integer.parseInt(input);
                        Main.imprimirIncompatibilidades1(solver.listaIncompatibilidades, ID);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Ingrese un valor numérico válido para el ID.");
                    }
                }
            }
        });
        // Agregar el botón a la ventana
        getContentPane().add(botonIncompatibilidades);
        // Mostrar la ventana
        setVisible(true);

        
        add(nombreLabel);
        add(nombreField);
        add(categoriaLabel);
        add(categoriaField);
        add(nivelLabel);
        add(IDLabel);
        add(nivelField);
        add(new JLabel());
        add(agregarButton);
        add(new JLabel());
        add(agregarPariaButton);

        pack();
        setVisible(true);
    }

    private Trabajador buscarTrabajador(int id) {
        for (Trabajador trabajador : trabajadores) {
            if (trabajador.codigo ==id) {
                return trabajador;
            }
        }
        return null;
    }

    

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TrabajadorGUI();
            }
        });
    }
}