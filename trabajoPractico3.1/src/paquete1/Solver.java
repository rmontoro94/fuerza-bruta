package paquete1;

import java.util.*;


public class Solver implements Runnable {
	
	// todos los trabajadores
	 Trabajador [] _personas; 
	
	// Set auxiliar para ir chequeando y elaborando los equipos
	private Set<Trabajador> equipoActual;
	
	// Set en el que se devolverá el mejor equipo encontrado
	private Set<Trabajador> equipoFinal;
	
	HashMap<Trabajador, Set<Trabajador>> listaIncompatibilidades;
	
	
	// Los siguientes dos valores determinan el tamaño del grupo y las vacantes por puesto de trabajo.
	// Se dejaron hardcodeados para simplificar, pero podrian añadirse al constructor en caso de querer escalar
	// el programa y permitir grupos de distinto tamaño y con distinto numero de vacantes
	// Ademas, se podria separar la variable "cantEmpleos" en varias distintas para cada tipo de empleo, para asi
	// poder tener una cantidad de vacantes especificas para cada puesto.
	// Determinamos que eso terminaba en un constructor demasiado enmarañado, así que preferimos dejarlo de manera 
	// simple. Pero el caso fue considerado.
	
	// la cantidad de trabajadores que se quieren para cada puesto. Se dejo en un valor de 1
	int cantEmpleos = 1;
	int tamañoDelGrupo = 4;
	
	
	// contador para guardar todas las combinaciones ejecutadas
	int contadorGenerados = 0;
	
	public Solver( Trabajador[] lasPersonas ) {
		
		_personas = lasPersonas;
		equipoActual  = new HashSet<Trabajador>();
		equipoFinal = new HashSet<Trabajador>();
		listaIncompatibilidades = crearListaIn(lasPersonas);
		
	}
	
	Set<Trabajador> generameEquipo(int pos){
		
		if(equipoActual.size() == tamañoDelGrupo) {
			
			if(grupoValido(equipoActual, cantEmpleos, cantEmpleos, cantEmpleos, cantEmpleos)) {
				
				if(grupoCompatible(equipoActual)) {
					
					if(valorEquipo(equipoActual) > valorEquipo(equipoFinal)) {
						
						equipoFinal = clonarEquipo(equipoActual);
					}
					
				}
				
			}
			
		}
		
		if(pos == _personas.length) {
			
			// se contabilizan TODAS las combinaciones posibles generadas (incluso si estas son una respuesta de tamaño mayor al deseado)
			contadorGenerados++;
			
		}else {
			
			equipoActual.add(_personas[pos]);
			generameEquipo(pos + 1);
			
			equipoActual.remove(_personas[pos]);
			generameEquipo(pos + 1);
			
		}
		
		return equipoFinal;
	}
	
	public int getContadorGenerados() {
		return contadorGenerados;
	}
	
	 int valorEquipo(Set<Trabajador> equipo) {
		
		int valorEquipo =0;
		
		for(Trabajador trabajador: equipo) {
			
			valorEquipo = valorEquipo + trabajador.calificacion;
		}
		
		return valorEquipo;
	}
	 
	
	 // este es un metodo muy sencillo para clonar.
	 Set<Trabajador> clonarEquipo(Set<Trabajador> equipo){
		
		Set<Trabajador>equipoFinal = new HashSet<Trabajador>();
		
		for(Trabajador laburante: equipo) {
			
			equipoFinal.add(laburante);
		}
		
		return equipoFinal;
		
	}
	 @Override
	    public void run() {
	        generameEquipo(0);
	    }
	 
	
	
	 boolean grupoCompatible(Set<Trabajador> equipo) {
		boolean resultado = true;
		
		for(Trabajador sujeto: equipo) {
			
			for(Trabajador elOtro: equipo) {
				
				if(!sujeto.equals(elOtro)) {
					
					if(sujeto.incompatibles.contains(elOtro)) {
						
						resultado = false;
					}
				}
			}	
		}
		return resultado;
	}
	 
	  boolean grupoValido(Set<Trabajador> equipo, int lideres, int arquitectos, int programadores, int testers) {
		    int contadorLideres = 0;
		    int contadorArquitectos = 0;
		    int contadorProgramadores = 0;
		    int contadorTesters = 0;

		    for (Trabajador persona : equipo) {
		        if (persona.getRol().equals("Lider de proyecto")) {
		            contadorLideres++;
		        } else if (persona.getRol().equals("Arquitecto")) {
		            contadorArquitectos++;
		        } else if (persona.getRol().equals("Programador")) {
		            contadorProgramadores++;
		        } else if (persona.getRol().equals("Tester")) {
		            contadorTesters++;
		        }
		    }

		    return contadorLideres >= lideres && contadorArquitectos >= arquitectos && contadorProgramadores >= programadores && contadorTesters >= testers;
		}

	  private HashMap<Trabajador, Set<Trabajador>> crearListaIn(Trabajador[] laLista) {
		 
		 HashMap<Trabajador, Set<Trabajador>> respuesta = new HashMap<Trabajador, Set<Trabajador>>();
		 
		 for(Trabajador sujeto: laLista) {
			 
			 respuesta.put(sujeto, sujeto.incompatibles);
		 }
		 
		 return respuesta;
	 }
	 
	  String requerimientos() {
		  
		  String respuesta = "El equipo consta de " + tamañoDelGrupo + " personas y hay " + cantEmpleos + " vacantes por rol";
		  return respuesta;
	  }
	 
}
