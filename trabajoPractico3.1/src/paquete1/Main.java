package paquete1;

import java.util.*;
import java.util.Map.Entry;

public class Main {
	
	static void imprimirTrabajadores(Trabajador[] trabajadores) {
		
		System.out.println("Los trabajadores son:");
		for(Trabajador trabajador: trabajadores) {
			
			System.out.println(trabajador);
		}
	}
	
	static void imprimirIncompatibilidades(HashMap<Trabajador, Set<Trabajador>> trabajadores) {
		
		System.out.println("Las incompatibilidades son:");
		for(Map.Entry<Trabajador, Set<Trabajador>> set: trabajadores.entrySet()  ){
			
			System.out.println("Sujeto : " + set.getKey().toString() + "Incompatibles:  " + set.getValue().toString() );
		}
		
	}
	
static Set<Trabajador> imprimirIncompatibilidades1(HashMap<Trabajador, Set<Trabajador>> trabajadores, int codigo2) {
		
	Set<Trabajador> incompatibles = new HashSet<>();

    for (Map.Entry<Trabajador, Set<Trabajador>> entry : trabajadores.entrySet()) {
        Trabajador trabajador = entry.getKey();
        if (trabajador.codigo == codigo2) {
            incompatibles = entry.getValue();
            break;
        }
    }

    return incompatibles;
}
		
	

	
	static void imprimirRequerimientosEquipo(Solver elSolver) {
		
		String respuesta = elSolver.requerimientos();
		System.out.println(respuesta);
	}
	
	static void imprimirCombinacionesGeneradas(Solver elSolver) {
		
		int a = elSolver.getContadorGenerados();
		System.out.println("Se probaron: " + a + " combinaciones");
	}
	
	static void imprimirMejorEquipo(Solver solver) {
		
		// se dejo que en en los equipos se tenga en cuenta siempre desde el primer trabajador (posicion 0) y que haga
		// equipos de 4 personas (el 4)
		Solver equipoGol =  solver;
		
		System.out.println("Mejor equipo:" + equipoGol.generameEquipo(0));
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<Trabajador> parias = new HashSet<Trabajador>();
		
		Trabajador malo1 = new Trabajador("fede", 5, "programador", parias, 1000);
		Trabajador malo2 = new Trabajador("papu", 5, "programador", parias, 10001);
		parias.add(malo1);
		parias.add(malo2);
		
		Trabajador[] todosLTrabajadores = new Trabajador[8];
		
		todosLTrabajadores[0] = new Trabajador("pedro", 3, "Lider de proyecto", parias, 10002);
		todosLTrabajadores[1] = new Trabajador("rodri", 2, "Arquitecto", parias, 10003);
		todosLTrabajadores[2] = new Trabajador("pato", 1, "Programador", parias, 10004);
		todosLTrabajadores[3] = new Trabajador("papa", 4, "Tester", parias, 10005);
		todosLTrabajadores[4] = new Trabajador("beto", 5, "Programador", parias, 10006);
		todosLTrabajadores[5] = new Trabajador("juan", 5, "Lider de proyecto", parias, 10007);
		todosLTrabajadores[6] = malo1;
		todosLTrabajadores[7] = malo2;
		
		Solver solver = new Solver(todosLTrabajadores);
        Thread thread = new Thread(solver);
        //thread.start();
        
		
		imprimirRequerimientosEquipo(solver);
		imprimirTrabajadores(solver._personas);
		//imprimirIncompatibilidades(solver.listaIncompatibilidades);
		imprimirMejorEquipo(solver);
		imprimirCombinacionesGeneradas(solver);
	}

}
